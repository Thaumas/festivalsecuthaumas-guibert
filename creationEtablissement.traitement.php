<?php

include_once '_gestionBase.inc.php';

/*
 * V1 Simple
 */

//if (isset($_REQUEST)) {
//    $id = $_REQUEST['id'];
//    $nom = $_REQUEST['nom'];
//    $adresseRue = $_REQUEST['adresseRue'];
//    $adressePostale = $_REQUEST['codePostal'];
//    $ville = $_REQUEST['ville'];
//    $tel = $_REQUEST['tel'];
//    $adresseElectronique = $_REQUEST['adresseElectronique'];
//    $type = $_REQUEST['type'];
//    $civiliteResponsable = $_REQUEST['civiliteResponsable'];
//    $nomResponsable = $_REQUEST['nomResponsable'];
//    $prenomResponsable = $_REQUEST['prenomResponsable'];
//    $nombreChambresOffertes = $_REQUEST['nombreChambresOffertes'];
//
//    $reussi = creerEtablissement($id, $nom, $adresseRue, $adressePostale, $ville, $tel, $adresseElectronique, $type, $civiliteResponsable, $nomResponsable, $prenomResponsable, $nombreChambresOffertes);
//}

/*
 * V2 avec filter
 */
$filtre = array(
    'id' => FILTER_SANITIZE_STRING,
    'nom' => FILTER_SANITIZE_STRING,
    'adresseRue' => FILTER_SANITIZE_STRING,
    'ville' => FILTER_SANITIZE_STRING,
    'codePostal' => FILTER_SANITIZE_NUMBER_INT,
    'tel' => FILTER_SANITIZE_NUMBER_INT,
    'adresseElectronique' => FILTER_SANITIZE_EMAIL,
    'type' => FILTER_SANITIZE_NUMBER_INT,
    'civiliteResponsable' => FILTER_SANITIZE_STRING,
    'nomResponsable' => FILTER_SANITIZE_STRING,
    'prenomResponsable' => FILTER_SANITIZE_STRING,
    'nombreChambresOffertes' => FILTER_SANITIZE_NUMBER_INT
);

$etablissement = filter_input_array(INPUT_POST, $filtre);

if ($etablissement != false) {
    creerEtablissement($etablissement["id"], $etablissement["nom"], $etablissement["adresseRue"], $etablissement["codePostal"], $etablissement["ville"], $etablissement["tel"], $etablissement["adresseElectronique"], $etablissement["type"], $etablissement["civiliteResponsable"], $etablissement["nomResponsable"], $etablissement["prenomResponsable"], $etablissement["nombreChambresOffertes"]);
    header("Location:consultationEtablissements.php");
} else {
    /*
     * Exemple de traitement de redirection et d'une sauvegarde de message d'erreurs dans un fichier de log
     */
    header("Location:avertissement.php?err=filtreCreationEtablissement");
    error_log(date("d.m.y") . " creationEtablissement.traitement.php : filtre PHP");
}

?>
