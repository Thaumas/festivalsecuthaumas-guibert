<?php
include_once '_debut.inc.php';
?>

<!-- Une div contenant la class "container" préfixe obligatoirement les lignes (div de class=row) -->
<div class="container">
    <!-- ligne principale -->
    <div class="row "> 

        <?php include_once '_menuGauche.inc.php'; ?>

        <!-- deuxième colonne (s'étend sur 7 colonnes sur 12 possibles à partir de la 3) -->
        <div class="col-md-7 ">
            <br />
            <!-- une ligne dans une colonne -->
            <div class="row">
                <div class="panel panel-danger">
                    <div class="panel-heading"></div>
                    <div class="panel-body">
                        <form>
                            <div class="form-group">
                                <label for="etablissements">Etablissements</label>
                                <select id="etablissements" class="form-control input-sm ">
                                    <?php
                                    $listeEtablissements = disponibiliteEtablissement();
                                    foreach ($listeEtablissements as $etablissement):

                                        $nom = $etablissement["nom"];
                                        $nombreChambresOffertes = $etablissement["nombreChambresOffertes"];
                                        $nombreChambresAttribuees = $etablissement["nbChambresAttribuees"];
                                        if ($etablissement["nbChambresDisponibles"] == null) {
                                            $nombreChambresDisponibles = $etablissement["nombreChambresOffertes"];
                                        } else {
                                            $nombreChambresDisponibles = $etablissement["nbChambresDisponibles"];
                                        }
                                        if ($nombreChambresDisponibles <= 0):
                                            ?>
                                            <option disabled value="<?php echo $etablissement["id"]; ?>">
                                                <?php echo "$nom (disponible : $nombreChambresDisponibles)" ?>
                                            </option>
                                        <?php else: ?>
                                            <option value="<?php echo $etablissement["id"]; ?>">
                                                <?php echo "$nom (disponible : $nombreChambresDisponibles)" ?>
                                            </option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                                <label for="groupes">Groupes</label>
                                <select id="groupes" class="form-control input-sm ">
                                    <?php
                                    $listeGroupe = hebergementEnAttente();
                                    foreach ($listeGroupe as $groupe):
                                        $id = $groupe["id"];
                                        $nomGroupe = $groupe["nom"];
                                        $nombrePersonnes = $groupe["nombrePersonnes"];
                                        if ($groupe["nbPersonnesEnAttente"] == null) {
                                            $nbPersonnesEnAttente = $groupe["nombrePersonnes"];
                                        } else {
                                            $nbPersonnesEnAttente = $groupe["nbPersonnesEnAttente"];
                                        }

                                        $nbPersonnesAffectes = $groupe["nbPersonnesAffectes"];
                                        if ($nbPersonnesEnAttente <= 0):
                                            ?>
                                            <option disabled value="<?php echo $id ?>">
                                                <?php echo "$nomGroupe (en attente: $nbPersonnesEnAttente )" ?>
                                            </option>
                                        <?php else: ?>
                                            <option value="<?php echo $id ?>">
                                                <?php echo "$nomGroupe (en attente: $nbPersonnesEnAttente )" ?>
                                            </option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>

                                <label for="attribution" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="number" max="<?php echo $nbPersonnesEnAttente ; ?>" name="nbAttrib" class="form-control" id="attribution" placeholder="Email">
                                </div>
                                <button type="button" class="btn btn-success">Affecter</button>
                            </div>
                        </form>
                    </div>





                </div>
            </div>
        </div>



    </div> <!-- /container -->

    <?php include("_fin.inc.php"); ?>
